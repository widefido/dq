package main

import (
	"bitbucket.org/widefido/dq/server"

	"runtime"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	s, err := server.NewQueueServer("data")
	if err != nil {
		panic(err)
	}
	defer s.Close()

	s.Serve()
}
