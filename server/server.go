package server

import (
	"bitbucket.org/widefido/red/server"

	"bitbucket.org/widefido/dq/queue"

	"sort"
	"sync"
	"time"
)

type QueueServer struct {
	dataDir      string
	queueMap     map[string]queue.BackendQueue
	queueMapLock *sync.RWMutex
	server       *server.Server
}

func NewQueueServer(dataDir string) (*QueueServer, error) {
	config := server.DefaultServerConfig()
	config.Name = "queue server"

	s, err := server.New(config)
	if err != nil {
		return nil, err
	}

	qs := &QueueServer{
		dataDir:      dataDir,
		queueMap:     make(map[string]queue.BackendQueue),
		queueMapLock: &sync.RWMutex{},
		server:       s,
	}

	s.RegisterHandlers([]server.CommandHandler{
		server.CommandHandlerFunc("KEYS", "KEYS", 0, false, qs.HandleKeys),
		server.CommandHandlerFunc("PUSH", "PUSH key message", 2, true, qs.HandlePush),
		server.CommandHandlerFunc("POP", "POP key [timeout]", -1, true, qs.HandlePop),
		server.CommandHandlerFunc("DEPTH", "DEPTH key", 1, true, qs.HandleDepth),
		server.CommandHandlerFunc("FLUSH", "FLUSH key", 1, true, qs.HandleFlush),
	}...)

	return qs, nil
}

func (self *QueueServer) Serve() error {
	return self.server.Serve()
}

func (self *QueueServer) Stop() {
	self.server.Stop()
}

func (self *QueueServer) Close() error {
	self.queueMapLock.Lock()
	defer self.queueMapLock.Unlock()

	for _, queue := range self.queueMap {
		queue.Close()
	}

	return self.server.Close()
}

func (self *QueueServer) GetQueue(key string) queue.BackendQueue {
	self.queueMapLock.RLock()
	q, ok := self.queueMap[key]
	self.queueMapLock.RUnlock()
	if ok {
		return q
	}

	self.queueMapLock.Lock()
	defer self.queueMapLock.Unlock()
	q = queue.NewDiskQueue(key, self.dataDir, 1*1024*1024, -1, time.Second*60, nil)
	self.queueMap[key] = q
	return q
}

func (self *QueueServer) HandleKeys(client *server.Client, request *server.Request) error {
	self.queueMapLock.RLock()
	defer self.queueMapLock.RUnlock()

	keys := make([]string, 0)
	for k, q := range self.queueMap {
		if q.Depth() <= 0 {
			continue
		}

		keys = append(keys, k)
	}

	sort.Strings(keys)

	client.SendReply(keys)
	return nil
}

func (self *QueueServer) HandlePush(client *server.Client, request *server.Request) error {
	key, err := request.ArgString(0)
	if err != nil {
		return err
	}

	message, err := request.ArgBytes(1)
	if err != nil {
		return err
	}

	err = self.GetQueue(key).Put(message)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

func (self *QueueServer) HandlePop(client *server.Client, request *server.Request) error {
	key, err := request.ArgString(0)
	if err != nil {
		return err
	}

	timeout, err := request.ArgInt64(1)
	if err != nil {
		timeout = 1000
	}

	messageCh := self.GetQueue(key).ReadChan()
	timeoutCh := time.After(time.Millisecond * time.Duration(timeout))

	for {
		select {
		case message := <-messageCh:
			client.SendReply(message)
			return nil
		case <-timeoutCh:
			if timeout > 0 {
				client.SendNil()
				return nil
			}
		}
	}
	return nil
}

func (self *QueueServer) HandleDepth(client *server.Client, request *server.Request) error {
	key, err := request.ArgString(0)
	if err != nil {
		return err
	}

	client.SendReply(self.GetQueue(key).Depth())
	return nil
}

func (self *QueueServer) HandleFlush(client *server.Client, request *server.Request) error {
	key, err := request.ArgString(0)
	if err != nil {
		return err
	}

	err = self.GetQueue(key).Empty()
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}
