TEST=.
BENCH=.
COVERPROFILE=/tmp/c.out
BRANCH=`git rev-parse --abbrev-ref HEAD`
COMMIT=`git rev-parse --short HEAD`
CWD=`pwd`
GOLDFLAGS="" #-X main.branch $(BRANCH) -X main.commit $(COMMIT)"
GOPATH=$(CWD)/.gopath
GOBUILDFLAGS="-a"
GOGCFLAGS=""
CGO_CFLAGS=""
CGO_LDFLAGS=""

default: build

clean:
	@rm -f bin/*

# http://cloc.sourceforge.net/
cloc:
	@cloc --by-file-by-lang --exclude-dir=.git,env32,env33,env34,.gopath --not-match-f='Makefile|_test.go' .

fmt:
	@GOPATH=$(GOPATH) go fmt ./...

get:
	@GOPATH=$(GOPATH) go get -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) go get -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) go get -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) go get -a -d -v ./...

get-update:
	@GOPATH=$(GOPATH) go get -a -u -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) go get -a -u -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) go get -a -u -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) go get -a -u -d -v ./...

gopath:
	@mkdir -p $(GOPATH)/src/bitbucket.org/widefido
	@ln -s ../../../../ $(GOPATH)/src/bitbucket.org/widefido/dq

bench: fmt
	@echo "=== BENCHMARKS ==="

test: fmt
	@echo "=== TESTS ==="

cover: fmt
	GOPATH=$(GOPATH) go test -coverprofile=$(COVERPROFILE) -test.run=$(TEST) $(COVERFLAG) .
	GOPATH=$(GOPATH) go tool cover -html=$(COVERPROFILE)
	rm $(COVERPROFILE)

vet:
	GOPATH=$(GOPATH) go vet ./...

todo:
	@git grep --untracked TODO

dq: get fmt 
	@mkdir -p bin
	@GOPATH=$(GOPATH) go build $(GOBUILDFLAGS) -gcflags=$(GOGCFLAGS) -ldflags=$(GOLDFLAGS) -o bin/dq ./cmd/dq/main.go

build: get fmt

all: build

.PHONY: clean bench cloc cover fmt get build test
